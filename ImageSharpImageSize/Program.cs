﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using System.Diagnostics;
using System.Linq;

namespace ImageSharpImageSize;

class Program
{
    record ImageSize(int Width, int Height);
    static async Task Main(string[] args)
    {
        var imageSizes = new List<ImageSize>
        {
            new(1080, 1920),
            new(1919, 1080),
            new(1080, 900),
            new(1080, 500)
        };

        Console.WriteLine("Original Sizes");
        imageSizes.ForEach(i => Console.WriteLine($"Size {i.Width}x{i.Height}px"));
        Console.WriteLine();
        imageSizes.Select(i => SimulateResize(i.Width, i.Height)).ToList().ForEach(tuple => Console.WriteLine($"Size {tuple.Width}x{tuple.Height}px"));
    }

    private static ImageSize SimulateResize(int width, int height)
    {
        const int MaxWidth = 640;

        double resizeFactor = Math.Max(width, height) / (double)MaxWidth;
        if (resizeFactor < 1)
        {
            resizeFactor = 1;
        }

        return new((int)(width / resizeFactor), (int)(height / resizeFactor));
    }

    private static async Task HalfImageSizeTests()
    {
        Stopwatch s = new Stopwatch();
        s.Start();
        await HalfImageSize
            (
            @"C:\Users\danny\source\repos\gitlab\ImageSharpImageSize\ImageSharpImageSize\images\pepsiDog.png",
            @"C:\Users\danny\source\repos\gitlab\ImageSharpImageSize\ImageSharpImageSize\images\out\pepsiDog.jpg"
            );
        s.Stop();
        Console.WriteLine($"{s.ElapsedMilliseconds} ms spent on pepsiDog.png{Environment.NewLine}");

        s.Restart();
        await HalfImageSize
            (
            @"C:\Users\danny\source\repos\gitlab\ImageSharpImageSize\ImageSharpImageSize\images\cosplay.png",
            @"C:\Users\danny\source\repos\gitlab\ImageSharpImageSize\ImageSharpImageSize\images\out\cosplay.jpg"
            );
        s.Stop();
        Console.WriteLine($"{s.ElapsedMilliseconds} ms spent on cosplay.png{Environment.NewLine}");
    }

    static (int height, int width) GetImageDimensions(Image image)
        => (image.Height, image.Width);

    static void PrintDimensions(Image image, string name)
    {
        var (height, width) = GetImageDimensions(image);
        Console.WriteLine($"{ name}'s dimensions are {width}x{height}px");
    }

    static async Task HalfImageSize(string inFilePath, string outFilePath)
    {
        using Image img = Image.Load(inFilePath);
        PrintDimensions(img, nameof(img));

        int width = img.Width / 2;
        int height = img.Height / 2;
        img.Mutate(i => i.Resize(width, height));
        PrintDimensions(img, nameof(img));
        await img.SaveAsJpegAsync(outFilePath);
    }
}