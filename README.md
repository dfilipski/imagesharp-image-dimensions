# ImageSharpImageSize
This is an example project using the ImageSharp library to determine the dimensions of an image.

##  Image Credits
- "rooster" by TALMADGEBOYD is marked with CC BY 2.0. To view the terms, visit https://creativecommons.org/licenses/by/2.0/?ref=openverse 
- "Rooster" by -JvL- is marked with CC BY 2.0. To view the terms, visit https://creativecommons.org/licenses/by/2.0/?ref=openverse